package model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Product {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private double price;
    private String unit;
    private boolean isPromo;
    private String promoDetails;
    private String image;
}

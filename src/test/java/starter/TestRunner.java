package starter;

import io.cucumber.junit.CucumberOptions;
import io.restassured.RestAssured;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features"
)
public class TestRunner {
    @BeforeClass
    public static void setUp() {
        Properties properties = new Properties();
        try (InputStream outputStream = TestRunner.class.getClassLoader().getResourceAsStream("config.properties")) {
            properties.load(outputStream);
            RestAssured.baseURI = properties.getProperty("base.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
package starter.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import model.Detail;
import model.Error;
import model.Product;
import net.serenitybdd.rest.SerenityRest;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ResponseUtils {
    public static ArrayList<Product> getSuccessResponse() {
        if (SerenityRest.lastResponse().statusCode() != 200) throw new RuntimeException("Request failed");
        Type userListType = new TypeToken<ArrayList<Product>>() {}.getType();
        return new Gson().fromJson(SerenityRest.lastResponse().getBody().asPrettyString(), userListType);
    }

    public static Detail getErrorResponse() {
        if (SerenityRest.lastResponse().statusCode() != 404) throw new RuntimeException("Request does not failed");
        Error error = new Gson().fromJson(SerenityRest.lastResponse().getBody().asPrettyString(), Error.class);
        return error.detail();
    }
}
package starter.stepdefinitions;

import io.cucumber.java8.En;
import model.Detail;
import model.Product;
import net.serenitybdd.rest.SerenityRest;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static starter.utils.ResponseUtils.getErrorResponse;
import static starter.utils.ResponseUtils.getSuccessResponse;

public class ProductDefinitions implements En {

    public ProductDefinitions() {
        When("^he calls endpoint to get \"([^\"]*)\"$", (String product) -> {
            SerenityRest.given().get(product);
        });

        Then("^he sees the results displayed for \"([^\"]*)\"$", (String product) -> {
            ArrayList<Product> successResponse = getSuccessResponse();
            assertThat(successResponse.size()).isNotZero();
            assertTrue(successResponse.stream().map(Product::getTitle).anyMatch(title -> title.contains(product)));
        });

        Then("^he does not see the results with \"([^\"]*)\"$$", (String product) -> {
            Detail errorResponse = getErrorResponse();
            assertTrue(errorResponse.error());
            assertThat(errorResponse.requestedItem()).isEqualTo(product);
        });
    }
}

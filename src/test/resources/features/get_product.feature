Feature: Get product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario Outline: Validate success results by specific product
    When he calls endpoint to get "<product>"
    Then he sees the results displayed for "<product>"
    Examples:
      | product |
      | apple   |
      | mango   |
      | tofu    |
      | water   |

  Scenario: Validate error results by specific product
    When he calls endpoint to get "car"
    Then he does not see the results with "car"
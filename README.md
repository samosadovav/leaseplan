# Getting started with Leaseplan automation project

## How to run locally 
* Right-click on the TestRunner.java class and select Run 'TestRunner'.
```path 
path: src/test/java/starter/TestRunner.java 
```
* Go to gherkin feature files and click run under scenario line
```path 
path: src/test/resources/features/post_product.feature 
```

## How to Run from command line
Open command line and go to the root of the project and execute:
```gradle
./gradlew test
```
## How to check the report
Open **index.htm** file in any browser
```path 
path: target/site/serenity/index.html
```

## How to verify CI/CD result
Go to CI/CD -> Piplines -> click on last pipline id link -> jobs
* In the end of the test line you can download artifacts.zip file.
Unzip file and open ```target/site/serenity/index.html ```
* Click on the Test link -> from right side under job artifacts section 
  click browse - target - site - serenity - index.html
  

# [Link how to add new tests](https://cucumber.io/docs/cucumber/step-definitions/)